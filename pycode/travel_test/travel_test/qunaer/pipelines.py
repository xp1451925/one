# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import pandas as pd
# useful for handling different item types with a single interface
from itemadapter import ItemAdapter


class TravelTestPipeline:
    def process_item(self, item, spider):
        df=pd.DataFrame(item)
        df.to_excel('去哪儿景点信息.xlsx',index=False)
        df.to_csv('去哪儿景点信息.csv',index=False)
        return item
