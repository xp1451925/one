import re
import scrapy
from bs4 import BeautifulSoup
from items import TravelTestItem



class QunaerSpider(scrapy.Spider):
    name = "qunaer"
    allowed_domains = ["travel.qunar.com"]
    types = ['p-cs299914-beijing', 'p-cs299782-xiamen', 'p-cs299878-shanghai', 'p-cs300195-hangzhou',
             'p-cs299861-nanjing', 'p-cs300085-chengdu', 'p-cs300079-lijiang', 'p-cs300027-xianggang', 'p-cs300028-aomen',
             'p-cs300002-taibei', 'p-cs300188-sanya', 'p-cs300100-xian', 'p-cs299801-guilin', 'p-cs299979-chongqing',
             'p-cs299783-qingdao', 'p-cs299937-suzhou', 'p-cs300134-dalian', 'p-cs300132-guangzhou',
             'p-cs300118-shenzhen', 'p-cs300064-zhangjiajie']

    def start_requests(self):
        for t in self.types:
            for i in range(30):
                url = 'https://travel.qunar.com/{}-jingdian-1-{}'.format(t, i)
                yield scrapy.Request(url, callback=self.parse, meta={'type': t, 'page': i})

    def parse(self, response):
        links = self.get_subpage_links(response.text)
        for link in links:
            yield response.follow(link, callback=self.parse_subpage)

    def parse_subpage(self, response):
        html = response.text
        soup = BeautifulSoup(html, 'html.parser')
        # 提取数据
        title = self.get_title(soup)
        score = self.get_score(soup)
        opening_hours = self.get_opening_hours(soup)
        address = self.get_address(soup)
        season = self.get_season(soup)
        transportation = self.get_transportation(soup)
        tips = self.get_tips(soup)
        lowest_price = self.get_lowest_price(soup)
        user, review, image_count = self.get_user_review(soup)
        item=TravelTestItem()
        # 输出数据
        item['title'] = title
        item['score'] = score
        item['opening_hours'] = opening_hours
        item['address'] = address
        item['season'] = season
        item['transportation'] = transportation
        item['tips'] = tips
        item['lowest_price'] = lowest_price
        item['user'] = user
        item['review'] = review
        item['image_count'] = image_count
        yield item

    def get_subpage_links(self, html):
        pattern = r'<a data-beacon="poi" href="(.*?)"'
        links = re.findall(pattern, html, re.S)
        return links

    def get_title(self, soup):
        title = soup.find('h1', class_='txtlink').text.strip()
        return title

    def get_score(self, soup):
        score = soup.find('span', class_='cur_score').text.strip()
        return score

    def get_opening_hours(self,soup):
        try:
            opening_hours_dd = soup.find('dl', class_='m_desc_right_col').find('p')
            if opening_hours_dd:
                return opening_hours_dd.text.strip()
        except:
            return ''

    # 提取地址
    def get_address(self,soup):
        address_div = soup.find('div', class_='e_summary_list_box')
        if address_div:
            address = address_div.find('dd').find('span')
            if address:
                return address.text.strip()
        return ''

    # 提取旅游时节
    def get_season(self,soup):
        soup_all = soup.find_all('div', class_='e_db_content_box e_db_content_dont_indent')
        try:
            season = soup_all[1].text
        except:
            season = ''
        return season

    # 提取交通指南
    def get_transportation(self,soup):
        soup_all = soup.find_all('div', class_='e_db_content_box e_db_content_dont_indent')
        try:
            transport = soup_all[2].text.strip()
        except:
            transport = ''
        return transport

    # 提取小贴士
    def get_tips(self,soup):
        soup_all = soup.find_all('div', class_='e_db_content_box e_db_content_dont_indent')
        try:
            # 提取小贴士
            tips = soup_all[3].text.strip()
        except:
            tips = ''
        return tips

    # 提取今日最低票价
    def get_lowest_price(self,soup):
        soup_all = soup.find_all('div', class_='e_db_content_box e_db_content_dont_indent')
        try:
            ticket = soup_all[0].text
        except:
            ticket = ''
        return ticket

    # 提取驴友点评中第一个用户的用户名、评论和图片数量
    def get_user_review(self,soup):
        try:
            # 提取驴友点评
            reviews = []
            review_list = soup.find_all('li', class_='e_comment_item clrfix')
            for review in review_list:
                content = review.find('div', class_='e_comment_content').text.strip()
                rating = review.find('div', class_='e_comment_usr_name').text.strip()
                cmt_img = review.find_all('img', class_='cmt_img')
                cmt_img_len = len(cmt_img)
                reviews.append({'rating': rating, 'content': content, 'lenbimg': cmt_img_len})
        except:
            reviews = []
        try:
            return reviews[0]['rating'], reviews[0]['content'], reviews[0]['lenbimg']
        except:
            return '', '', 0

    def get_subpage_links(self, html):
        pattern = r'<a data-beacon="poi" href="(.*?)"'
        links = re.findall(pattern, html, re.S)
        return links
