# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class TravelTestItem(scrapy.Item):
    title = scrapy.Field()
    score = scrapy.Field()
    opening_hours = scrapy.Field()
    address = scrapy.Field()
    season = scrapy.Field()
    transportation = scrapy.Field()
    tips = scrapy.Field()
    lowest_price = scrapy.Field()
    user = scrapy.Field()
    review = scrapy.Field()
    image_count = scrapy.Field()
    pass
