# Scrapy settings for Travel project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = "Travel"

SPIDER_MODULES = ["Travel.spiders"]
NEWSPIDER_MODULE = "Travel.spiders"


# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36"

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
CONCURRENT_REQUESTS = 16

# Configure a delay for requests for the same website (default: 0)
# See https://docs.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY = 2
RANDOMIZE_DOWNLOAD_DELAY =True
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
#    "Accept-Language": "en",
#}
# DEFAULT_REQUEST_HEADERS = {
#     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#     'Accept-Language': 'en',
#     'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36',
# }

DEFAULT_REQUEST_HEADERS = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'zh-CN,zh-TW;q=0.9,zh;q=0.8,en;q=0.7',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
    'Cookie': 'SECKEY_ABVK=AFhZMTFyfmCA8pq8Efwfpqfn4z/cTATyPEvbySNVT+g%3D; BMAP_SECKEY=BtE4JRRGILfHJE0a0D_PcGjD0XkliMp7vfqZK0RboT7J_vinsFvWPnwLT5j4FARy5zKan9JLfbnRCS7RORm-joGbBWBIuhnp_W6wGgB5XtFsy7lBC2AOJv2MskXnHiTQwlXjczphb2cZBmezfC0q1mClWNw7lnqahVVQdTr_fw6rsjJMeX6ohPV-fMl28QI2; QN1=0000908034fc52a60640de23; qunar-assist={%22version%22:%2220211215173359.925%22%2C%22show%22:false%2C%22audio%22:false%2C%22speed%22:%22middle%22%2C%22zomm%22:1%2C%22cursor%22:false%2C%22pointer%22:false%2C%22bigtext%22:false%2C%22overead%22:false%2C%22readscreen%22:false%2C%22theme%22:%22default%22}; QN277=partner; csrfToken=gYDXZLLa1rPIp3YqvTtKKw2UmL56BrB2; _i=VInJOEy3shxqWczqYcXil2X3JOtq; QN269=7CC76C30164611EEB415FA163E93D232; QN48=tc_7f8080bdd1a7ecad_1890a15aa76_c56b; fid=b62a4e78-946e-4778-a5c0-7fb6c8c4d7bc; QN71="MTgwLjIwOC41OC4xODg66IuP5beeOjE="; QN300=link.zhihu.com; ariaDefaultTheme=null; QN57=16880914229090.929853544412941; Hm_lvt_15577700f8ecddb1a927813c81166ade=1688091423; QN99=1956; QunarGlobal=10.66.93.173_-305a030_18909e222c8_-2502|1688091429725; QN601=0f8924e27d5ca1a5144e9d05e721abab; QN163=0; HN1=v15179216c350e81e7e8af23946dcd7609; HN2=quknngzgcrzcc; activityClose=1; QN100=WyLopb%2Flj4zniYjnurN86IuP5beeIiwi6KW%2F6JePfCJd; QN243=30; QN1231=0; QN63=%E4%BA%9A%E6%B4%B2%7C%E6%AC%A7%E6%B4%B2%7C%E8%8B%B1%E5%9B%BD; QN205=link.zhihu.com; QN67=13444%2C174792; _vi=JSTW9XVknY1NR9ZNOCOHu-jaU6bP7fP3AGfKIuZSi3Kpraq06ZYKO37GvU_SFoTTUtgly-AoJW5Syo8WbrSzhw1HGqg3qvEu9PmKKtc30IymdBmZd-2cOVu86nu2HE4CBIiLMKF9ta2o9jkFztIQmC13I6anvBvNKWNWBQhozdyX; QN267=0288421373943d23a8; QN58=1688107446287%7C1688111043794%7C11; JSESSIONID=35374AF29D4C47A16A6F32E60C70DCCD; Hm_lpvt_15577700f8ecddb1a927813c81166ade=1688111044; QN271=ba8624e2-1309-41c1-8a58-384c04911c0f; __qt=v1%7CVTJGc2RHVmtYMStyeVp5L3JaMUZSWXhnR2pRNHZ6R3JUZkxuVDRUeEY0VTFWL29HdW15T1piVWRzc3c1TjdrcDB1cHV1MHkrVnNIM2krbmc4NE52TTBmVnNjV0hjR0x6emswQnB6SlV2TTB2bnNRb0JsbDA3a1EzUEtKeVRSMFdud0gvZ2lacjJBeDM1SElHUlRBNEVMdTlvdWpaV1BjN2xOVVVnKzIzc0NFPQ%3D%3D%7C1688111130962%7CVTJGc2RHVmtYMTlhSmpZNFp5NXZTLy9RS1IxenBUc0k1VXhtR3ZhUEl2OFliWnViK09WYjBNTTc4ZHplN1B2VHRHdGhkT0UrVkpzdHMycXF3ZmwrMEE9PQ%3D%3D%7CVTJGc2RHVmtYMS9NU1pLak9XTVdTMFl5MWxldTRZWWtjSmJEeEpQRER6ZEZkQVFTYXRMQTZiMHFxbUxnZ3B4RkN4STBhTUpkOWZnZDlHNnc3MEIwa2dzcG92aGQvWE8xN2JMdUd3QTdWN0hqQXZLdjM3NWplb3Q5VkJMRkNUbHZ2ci95TGxWWlJXWnhqdGN1K2EwOWlWQkJSSlN5bUQ2aDBOT051VHFwNlRUb1UvRTRuUFMweDFCMmMyUy9DU1dKQ1YxOEJsOWNnZWY5SWFYaUpVQ0Q3T3NXVFU5d2Z4ZTFLdm1ybG5NVkF2VWRvaVQwYzdxODJKNzE1bUNYZDdSTmkrVTdmRS8vZldPOWhFUjZmTVBUODhzY25yKzNLRGdSajU1bDk0Q1FHRzgvaHQ3N1U2NUl0bDl4K3dBWnhrTGQxa2xhQ05ocG04Zmd5VU9JZ1ZRWW55MWw1eTEzazJkWUJHQVRJWkJWbGF4Q2tmdXBxL25HZzZsSHBIQjBzQXYzL2JLUERWc2hHb2I5UzZHU0J2RkFvanVhTm1IdGtTcENNQ3YvaDZhQWxvK203alJnVzBGanhKV0pUbFV5M1Rxbys2VHRMbE1hME1qV1ZrM0ljRUtveWdVWVh6LzZ2aWhqMkFCb3hjOEtHcldWWTZRZkFibWMrbWZqRzNSbUJrSU5zeFhiUUR4a0hFSFo1bHRFRTRCaUkrdHZ3ZE9aaDJmbWJVTDJLR1l6cVZIaWhRR0ppM041MGlpT002dTg0Z3Z1U01zZHptbCt2b0NWa3g1ZmVJV3ZaSGVNa2JoTXN4bGNWamszTVRremJYZk9lVVNtZ20xdlluZ1hvd0NOZmRVQkppUlVpQUdhdjhJYzl1VDVkN1dZSGRnT3EveGJQVldVSUsxWjZmYngvbkE9'
}

# Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    "Travel.middlewares.TravelSpiderMiddleware": 543,
#}

# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#DOWNLOADER_MIDDLEWARES = {
#    "Travel.middlewares.TravelDownloaderMiddleware": 543,
#}

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    "scrapy.extensions.telnet.TelnetConsole": None,
#}

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
#ITEM_PIPELINES = {
#    "Travel.pipelines.TravelPipeline": 300,
#}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = "httpcache"
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = "scrapy.extensions.httpcache.FilesystemCacheStorage"

# Set settings whose default value is deprecated to a future-proof value
REQUEST_FINGERPRINTER_IMPLEMENTATION = "2.7"
TWISTED_REACTOR = "twisted.internet.asyncioreactor.AsyncioSelectorReactor"
FEED_EXPORT_ENCODING = "utf-8"
