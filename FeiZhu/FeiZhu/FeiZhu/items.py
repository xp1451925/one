# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class FeizhuItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    Title = scrapy.Field()
    Intro = scrapy.Field()
    Price = scrapy.Field()
    Num_of_comment = scrapy.Field()

    pass
