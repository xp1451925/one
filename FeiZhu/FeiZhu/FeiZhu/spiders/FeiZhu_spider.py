# import scrapy
# from scrapy import Selector, cmdline
#
# from items import FeizhuItem
#
#
# class FeizhuSpiderSpider(scrapy.Spider):
#     name = "FeiZhu_spider"
#     allowed_domains = ["travelsearch.fliggy.com"]
#     start_urls = ["http://travelsearch.fliggy.com/"]
#     urls = "https://travelsearch.fliggy.com/index.htm?searchType=product&keyword=%E6%B1%9F%E6%B5%99%E6%B2%AA&-1=sales_des&pagenum={}&conditions=-1%3Asales_des"
#
#     def start_requests(self):
#         for i in range(1,101):
#             url = self.urls.format(i)
#             yield scrapy.Request(url=url,callback=self.parse)
#
#
#
#     def parse(self, response):
#         sel = Selector(response)
#
#         feizhu_list = sel.xpath('//div[@class="page-products-block clear-fix"]')
#         for feizhu in feizhu_list:
#             feizhu_item = FeizhuItem()
#
#             feizhu_item['Intro'] = sel.xpath('.//p[@class="other-msg"]/text').get()
#             feizhu_item['Price'] = sel.xpath('.//span[@class="price"]/text').get()
#             feizhu_item['Title'] = sel.xpath('.//span[@class="price"]').get()
#
#
#
#             print(feizhu_item)
#             yield feizhu_item
#
#
#
#
#
#
#
# if __name__ == '__main__':
#     cmdline.execute("scrapy crawl FeiZhu_spider -o feizhu_spider_result.csv".split())


























import scrapy
from scrapy import Selector, cmdline

from items import FeizhuItem


class FeizhuSpiderSpider(scrapy.Spider):
    name = "FeiZhu_spider"
    allowed_domains = ["travelsearch.fliggy.com"]
    start_urls = ["http://travelsearch.fliggy.com/"]
    urls = "https://travelsearch.fliggy.com/index.htm?searchType=product&keyword=%E6%B1%9F%E6%B5%99%E6%B2%AA&-1=sales_des&pagenum={}&conditions=-1%3Asales_des"

    def start_requests(self):
        for i in range(1, 101):
            url = self.urls.format(i)
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        sel = Selector(response)

        feizhu_list = sel.xpath('//div[@class="page-products-block clear-fix"]')
        for feizhu in feizhu_list:
            feizhu_item = FeizhuItem()

            feizhu_item['Intro'] = feizhu.xpath('.//p[@class="other-msg"]/text()').get()
            feizhu_item['Price'] = feizhu.xpath('.//span[@class="price"]/text()').get()
            feizhu_item['Title'] = feizhu.xpath('.//span[@class="price"]').get()

            print(feizhu_item)
            yield feizhu_item


if __name__ == '__main__':
    cmdline.execute("scrapy crawl FeiZhu_spider -o feizhu_spider_result.csv".split())



















