# Scrapy settings for FeiZhu project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = "FeiZhu"

SPIDER_MODULES = ["FeiZhu.spiders"]
NEWSPIDER_MODULE = "FeiZhu.spiders"


# Crawl responsibly by identifying yourself (and your website) on the user-agent
# USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36"

DEFAULT_REQUEST_HEADERS = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'zh-CN,zh-TW;q=0.9,zh;q=0.8,en;q=0.7',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
    'Cookie':'cna=ITi2GfemmmkCAXVQoPwLAaAE; xlly_s=1; t=1c3cb1686eaa0016f75c7de13ff43148; tracknick=tb265730315; lid=tb265730315; enc=cKBUr%2F0Qc0UJB7fM6ikvW19LGR6kLQh4cXPx2BqtRZ8RaRVBUE%2F8g879XP1ZHiathKd2hF5Lz8FNYhIk97ddiQGzewU8rDjOrgER%2FuoBGMQ2fjE%2BfeOf1%2BwVQ26DZfoH; _tb_token_=e535e3be3f15f; cookie2=141a25debb8120ac37c21036796ea009; dnk=tb265730315; uc1=cookie15=U%2BGCWk%2F75gdr5Q%3D%3D&cookie16=W5iHLLyFPlMGbLDwA%2BdvAGZqLg%3D%3D&cookie21=W5iHLLyFeDV1cfTOQA%3D%3D&cookie14=Uoe8gqLHVzhegg%3D%3D&pas=0&existShop=false; _l_g_=Ug%3D%3D; unb=4052466670; cookie1=VypS2SEBU6VULcSFI0LXGoXyOcvaBvxrW0w%2BUrv20w4%3D; login=true; cookie17=VyyUyYyZYZ8eMw%3D%3D; _nk_=tb265730315; sgcookie=E1009o%2Blyz0mLR08CNJ0WK8x4M%2FEoSQkgaF1HapatxYwLLfssjRD16J6sIv18pzPI61G%2FM1t1hbYMOsn64%2B5kDSyOMQydQO9gqtYKD8NpXxTckFDFWMD0P9P537r2zlWcOMB; cancelledSubSites=empty; sg=504; csg=315be987; SL_G_WPT_TO=zh-CN; SL_GWPT_Show_Hide_tmp=1; SL_wptGlobTipTmp=1; x5sec=7b22617365727665723b32223a22653335386662363737613337633635636339346566316436313833313265646543506953674b5547454a6d2f2b76534a6f732f6947426f4d4e4441314d6a51324e6a59334d4473784d4d7a533674722f2f2f2f2f2f77464141773d3d227d; _mw_us_time_=1688209793643; isg=BNnZ9S_82egum4VDnd4pVshw6MWzZs0YyYbSBfuOSYB_AvmUQ7I76EcTAMZ0vWVQ; l=fBg3rIf7N2VOj7EYBOfZPurza77TbIRA_uPzaNbMi9fP_Dfp50jCW1sAfvL9CnGVFsH2R3lDK4dwBeYBq3K-nxvtSFPEz4kmnmOk-Wf..; tfstk=dEGJY40FPnxovXLpZupDT7qiEaTDnb3yM0u1tkqldmnx72YPKUo3A6nn-bmnRe8p9ciNrJtzZXnxqmcnE60CJyi0NJkuxuqL92o9ohAMs4uzTJtMjCx3V4PF10ns4C0rzW7RAFDXsy3TZpBzFE8Wj_1qgXUWdPHx7aEj9ze8v6qA9T83y8EsPoKeYO-nsfQ2iGqT-TTvk9WUFrlFrK4N.'
}



# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://docs.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY = 3
RANDOMIZE_DOWNLOAD_DELAY = True
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
#    "Accept-Language": "en",
#}

# Enable or disable spider middlewares
# See https://docs.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    "FeiZhu.middlewares.FeizhuSpiderMiddleware": 543,
#}

# Enable or disable downloader middlewares
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#DOWNLOADER_MIDDLEWARES = {
#    "FeiZhu.middlewares.FeizhuDownloaderMiddleware": 543,
#}

# Enable or disable extensions
# See https://docs.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    "scrapy.extensions.telnet.TelnetConsole": None,
#}

# Configure item pipelines
# See https://docs.scrapy.org/en/latest/topics/item-pipeline.html
#ITEM_PIPELINES = {
#    "FeiZhu.pipelines.FeizhuPipeline": 300,
#}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://docs.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = "httpcache"
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = "scrapy.extensions.httpcache.FilesystemCacheStorage"

# Set settings whose default value is deprecated to a future-proof value
REQUEST_FINGERPRINTER_IMPLEMENTATION = "2.7"
TWISTED_REACTOR = "twisted.internet.asyncioreactor.AsyncioSelectorReactor"
FEED_EXPORT_ENCODING = "utf-8"
