
CREATE TABLE place_message (
  name varchar(100),
  time_open TEXT,
  address TEXT
);

CREATE TABLE travel_message (
  name varchar(100),
  point float,
  traffic_suggestion TEXT,
  season TEXT,
  tips TEXT,
  lowest_price TEXT
);

CREATE TABLE opinion (
  name varchar(100),
  users varchar(1000),
  comment TEXT,
  photo_number varchar(1000)
);


CREATE TABLE temp_table AS
SELECT DISTINCT *
FROM place_message;
DELETE FROM place_message;
ALTER TABLE place_message
ADD PRIMARY KEY (name);
INSERT INTO place_message
SELECT * FROM temp_table;



CREATE TABLE temp_table1 AS
SELECT DISTINCT *
FROM travel_message;
DELETE FROM travel_message;
ALTER TABLE travel_message
ADD PRIMARY KEY (name);
INSERT INTO travel_message
SELECT * FROM temp_table1;

CREATE TABLE temp_table2 AS
SELECT DISTINCT *
FROM opinion;
DELETE FROM opinion;
ALTER TABLE opinion
ADD PRIMARY KEY (name);
INSERT  INTO opinion
SELECT * FROM temp_table2;


ALTER TABLE place_message
ADD PRIMARY KEY (name);

ALTER TABLE travel_message
ADD PRIMARY KEY (name);

ALTER TABLE opinion
ADD PRIMARY KEY (name);
select * from opinion; 


select * from opinion;
SELECT * FROM travel_message WHERE point >= 4;
SELECT * FROM opinion WHERE comment LIKE '%好%';
SELECT season, AVG(point) AS average_rating
FROM travel_message
GROUP BY season;
SELECT * FROM travel_message WHERE traffic_suggestion LIKE '%公交%';
